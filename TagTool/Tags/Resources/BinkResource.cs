using TagTool.Common;
using TagTool.Serialization;

namespace TagTool.Tags.Resources
{
    [TagStructure(Name = "bink_resource", Size = 0x14)]
    public class BinkResource
    {
        public TagData Data;
    }
}