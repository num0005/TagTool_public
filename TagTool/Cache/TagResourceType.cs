﻿namespace TagTool.Cache
{
    public enum TagResourceType : sbyte
    {
        Collision,
        Bitmap,
        BitmapInterleaved,
        Sound,
        Animation,
        RenderGeometry,
        Bink,
        Pathfinding
    }
}