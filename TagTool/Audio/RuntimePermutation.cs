﻿using TagTool.Serialization;

namespace TagTool.Audio
{
    [TagStructure(Size = 0x1)]
    public class RuntimePermutationFlag
    {
        public sbyte Unknown;
    }
}