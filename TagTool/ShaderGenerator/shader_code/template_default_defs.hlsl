#ifndef Albedo
#define Albedo albedo_default
#endif
#ifndef Bump_Mapping
#define Bump_Mapping bump_mapping_off
#endif
#ifndef Alpha_Test
#define Alpha_Test alpha_test_none
#endif
#ifndef Specular_Mask
#define Specular_Mask specular_mask_no_specular_mask
#endif
#ifndef Material_Model
#define Material_Model material_model_diffuse_only
#endif
#ifndef Environment_Mapping
#define Environment_Mapping environment_mapping_none
#endif
#ifndef Self_Illumination
#define Self_Illumination self_illumination_off
#endif
#ifndef Blend_Mode
#define Blend_Mode blend_mode_opaque
#endif
#ifndef Parallax
#define Parallax parallax_off
#endif
#ifndef Misc
#define Misc misc_first_person_never
#endif
#ifndef Distortion
#define Distortion distortion_off
#endif
#ifndef Soft_Fade
#define Soft_Fade soft_fade_off
#endif
