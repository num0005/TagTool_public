#include "../helpers.hlsl"

#ifdef flag_albedo_chameleon_masked

float4 albedo_chameleon_masked(float2 texture_coordinate)
{
    return float4(1, 0, 0, 1); // Red not implemented output
}

#endif
