#include "../helpers.hlsl"

#ifdef flag_albedo_two_detail_black_point

float4 albedo_two_detail_black_point(float2 texcoord)
{
    float2 base_map_texcoord = ApplyXForm(texcoord, base_map_xform);
    float2 detail_map_texcoord = ApplyXForm(texcoord, detail_map_xform);
    float2 detail_map2_texcoord = ApplyXForm(texcoord, detail_map2_xform);

    float4 base_map_sample = tex2D(base_map, base_map_texcoord);
    float4 detail_map_sample = tex2D(detail_map, detail_map_texcoord);
    float4 detail_map2_sample = tex2D(detail_map2, detail_map2_texcoord);


    // This one seems to make zero sense so I'm just going to program it as it disassembled

    float4 c0 = float4(21.1120949, 1, 0.5, 0.00313080009);
    float4 c1 = float4(12.92, (5.0 / 12.0), 1.055, -0.055);

    float4 r0 = detail_map2_sample;
    float4 r1 = detail_map_sample;
    float4 r2 = base_map_sample;

    // This code is very similar to the strange bungie stuff
    // But that value of 21.1, no idea. I've seen it before
    // and just substtitudes this function out with the
    // regular disassembled version
    // except this one appears to be slightly different

    r1.xyz *= r2.xyz; //mul r1.xyz, r1, r2
    r0.xyz *= r1.xyz; //mul r0.xyz, r0, r1

    float3 export_color = r0.xyz;

    r1.xyz = r0.xyz * c0.x; //mul r1.xyz, r0, c0.x
    r2.x = c0.x; //mov r2.x, c0.x // Like what the fuck is a value like this doing in here?
    r0.xyz = r0.xyz * (-r2.xxx) + debug_tint.xyz; //mad r0.xyz, r0, -r2.x, c60
    r0.xyz = debug_tint.www * r0.xyz + r1.xyz; //mad r0.xyz, c60.w, r0, r1
    r1.xyz = log(r0.xyz); //log r1.x, r0.x //log r1.y, r0.y //log r1.z, r0.z
    r1.xyz *= c1.y; //mul r1.xyz, r1, c1.y
    r2.xyz = exp(r1.xyz); //exp r2.x, r1.x //exp r2.y, r1.y //exp r2.z, r1.z
    r1.xyz = r2.xyz * c1.z + c1.w; //mad r1.xyz, r2, c1.z, c1.w
    r2.xyz = (-r0.xyz) + c0.w; //add r2.xyz, -r0, c0.w // 50 shades of what the fuck is that
    r0.xyz *= c1.x; //mul r0.xyz, r0, c1.x
    float3 color = r2.xyz >= 0 ? r0.xyz : r1.xyz; //cmp oC0.xyz, r2, r0, r1

    // This is the other bit I dont get at all. Extra alpha processing.

    r0.x = r2.w + c0.y; //add r0.x, r2.w, c0.y
    r0.y = lerp(-r2.w, c0.y, c0.z); //lrp r0.y, c0.z, c0.y, -r2.w // I think this is some kind of average? c0.z = 0.5
    r0.x *= c0.z; //mul r0.x, r0.x, c0.z
    r0.z = r1.w * r0.w + (-r2.w); //mad r0.z, r1.w, r0.w, -r2.w
    r0.w = saturate(r1.w * r0.w + (-r0.x)); //mad_sat r0.w, r1.w, r0.w, -r0.x
    r0.y = 1.0 / r0.y; //rcp r0.y, r0.y
    r0.y = saturate(r0.y * r0.z); //mul_sat r0.y, r0.y, r0.z
    r0.x = r0.x * r0.y + r0.w; //mad r0.x, r0.x, r0.y, r0.w
    
    float alpha = r0.x;

    // In theory, any of the crazy stuff should just be compiled out?
    return float4(export_color, alpha);

}

//
// Generated by Microsoft (R) HLSL Shader Compiler 9.29.952.3111
//
// Parameters:
//
//   sampler2D base_map;
//   float4 base_map_xform;
//   float4 debug_tint;
//   sampler2D detail_map;
//   sampler2D detail_map2;
//   float4 detail_map2_xform;
//   float4 detail_map_xform;
//
//
// Registers:
//
//   Name              Reg   Size
//   ----------------- ----- ----
//   base_map_xform    c58      1
//   detail_map_xform  c59      1
//   debug_tint        c60      1
//   detail_map2_xform c61      1
//   base_map          s0       1
//   detail_map        s2       1
//   detail_map2       s3       1
//

//ps_3_0
//def c0, 21.1120949, 1, 0.5, 0.00313080009
//def c1, 12.9200001, 0.416666657, 1.05499995, -0.0549999997
//dcl_texcoord v0.xy
//dcl_texcoord1 v1
//dcl_2d s0
//dcl_2d s2
//dcl_2d s3
//mad r0.xy, v0, c61, c61.zwzw
//texld r0, r0, s3
//mad r1.xy, v0, c59, c59.zwzw
//texld r1, r1, s2
//mad r2.xy, v0, c58, c58.zwzw
//texld r2, r2, s0
//mul r1.xyz, r1, r2
//mul r0.xyz, r0, r1
//mul r1.xyz, r0, c0.x
//mov r2.x, c0.x
//mad r0.xyz, r0, -r2.x, c60
//mad r0.xyz, c60.w, r0, r1
//log r1.x, r0.x
//log r1.y, r0.y
//log r1.z, r0.z
//mul r1.xyz, r1, c1.y
//exp r2.x, r1.x
//exp r2.y, r1.y
//exp r2.z, r1.z
//mad r1.xyz, r2, c1.z, c1.w
//add r2.xyz, -r0, c0.w
//mul r0.xyz, r0, c1.x
//cmp oC0.xyz, r2, r0, r1
//add r0.x, r2.w, c0.y
//lrp r0.y, c0.z, c0.y, -r2.w
//mul r0.x, r0.x, c0.z
//mad r0.z, r1.w, r0.w, -r2.w
//mad_sat r0.w, r1.w, r0.w, -r0.x
//rcp r0.y, r0.y
//mul_sat r0.y, r0.y, r0.z
//mad r0.x, r0.x, r0.y, r0.w
//mov oC0.w, r0.x
//mov oC1.w, r0.x
//mad oC1.xyz, v1, c0.z, c0.z
//mov oC2, v1.w


#endif